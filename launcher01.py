import os
import datetime

config = ''
with open('/home/usr/git-repo/config01','r') as f:
    config = f.read()

print('CONFIG: {config}'.format(config=config))

with open('config','w') as f:
    f.write(config)

comment = str(datetime.datetime.now())

print('COMMENT: {comment}'.format(comment=comment))

with open('main.py','a') as f:
    f.write('\n#'+comment)

os.system('git add -A')
os.system('git commit -a -m "{comment}"'.format(comment=comment))
os.system('git push')